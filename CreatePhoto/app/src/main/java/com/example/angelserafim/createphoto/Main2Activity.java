package com.example.angelserafim.createphoto;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class Main2Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        DB myDB = new DB(this, "photoDB.db", null, 1);
        SQLiteDatabase SDB = myDB.getReadableDatabase();

        GridView gridView = (GridView) findViewById(R.id.gridView);

        if(SDB.isOpen()) {
            Cursor cursor = SDB.query("photoTable", new String[] {"photoColumn"}, null, null, null, null, null);

            int recordCurrent = 0;
            final String[] mImage = new String[cursor.getCount()];

            cursor.moveToFirst();
            while (cursor.getCount() != recordCurrent) {
                mImage[recordCurrent] = cursor.getString(cursor.getColumnIndex("photoColumn"));
                recordCurrent++;
                cursor.moveToNext();
            }
            cursor.close();

            gridView.setAdapter(new ImageAdapter(this, mImage));
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent main3Intent = new Intent(new Intent(Main2Activity.this, Main3Activity.class));

                    main3Intent.putExtra("id", position);

                    startActivity(main3Intent);
                }
            });
        }
        else
            Toast.makeText(Main2Activity.this, "БД не открыта", Toast.LENGTH_SHORT).show();

    }
}
