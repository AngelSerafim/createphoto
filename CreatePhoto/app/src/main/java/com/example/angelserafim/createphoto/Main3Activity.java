package com.example.angelserafim.createphoto;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class Main3Activity extends Activity {

    private float fromPosition, toPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        final ViewFlipper viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);

        Intent intent = getIntent();

        int id = intent.getIntExtra("id", 0);

        DB myDB = new DB(this, "photoDB.db", null, 1);
        SQLiteDatabase SDB = myDB.getReadableDatabase();

        if(SDB.isOpen()) {
            Cursor cursor = SDB.query("photoTable", new String[]{"photoColumn"}, null, null, null, null, null);

            ImageView imageView;
            String imageCurrent = "";
            int recordCurrent = 0;

            cursor.moveToFirst();
            while (cursor.getCount() != recordCurrent) {
                imageCurrent = cursor.getString(cursor.getColumnIndex("photoColumn"));

                imageView = new ImageView(this);

                imageView.setAdjustViewBounds(true);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setImageDrawable(Drawable.createFromPath(imageCurrent));

                viewFlipper.addView(imageView);

                recordCurrent++;
                cursor.moveToNext();
            }
            cursor.close();

            int i = 0;
            while(id != i) {
                viewFlipper.showNext();
                i++;
            }

            viewFlipper.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch(event.getAction()){
                        case MotionEvent.ACTION_DOWN:
                            fromPosition = event.getX();
                            break;
                        case MotionEvent.ACTION_UP:
                            toPosition = event.getX();
                            if(fromPosition > toPosition) {
                                viewFlipper.setInAnimation(Main3Activity.this, R.anim.in_from_right);
                                viewFlipper.setOutAnimation(Main3Activity.this, R.anim.out_to_left);
                                viewFlipper.showNext();
                            }
                            else if(fromPosition < toPosition) {
                                viewFlipper.setInAnimation(Main3Activity.this, R.anim.in_from_left);
                                viewFlipper.setOutAnimation(Main3Activity.this, R.anim.out_to_right);
                                viewFlipper.showPrevious();
                            }
                            break;
                        default:
                            break;

                    }

                    return true;
                }
            });
        }
    }

}
