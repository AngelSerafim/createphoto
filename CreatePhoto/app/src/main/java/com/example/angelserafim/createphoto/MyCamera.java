package com.example.angelserafim.createphoto;

import android.app.Activity;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyCamera extends Activity implements SurfaceHolder.Callback, View.OnClickListener, Camera.PictureCallback, Camera.PreviewCallback, Camera.AutoFocusCallback{

    private Camera camera;
    private SurfaceHolder surfaceHolder;
    private RelativeLayout relative;
    private SurfaceView preview;
    private Button startCam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_camera);
        relative = (RelativeLayout)findViewById(R.id.relativeLayout2);
        preview = (SurfaceView) findViewById(R.id.surfaceView);

        surfaceHolder = preview.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        startCam = (Button) findViewById(R.id.button);
        startCam.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera = Camera.open();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (camera != null){
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try{
            camera.setPreviewDisplay(holder);
            camera.setPreviewCallback(this);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        Camera.Size previewSize = camera.getParameters().getPreviewSize();

        //разобраться с соотношением сторон!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        float aspect = (float) previewSize.width / previewSize.height;

        int previewSurfaceWidth = preview.getWidth();
        int previewSurfaceHeight = preview.getHeight();

        ViewGroup.LayoutParams lp = preview.getLayoutParams();

        if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE)
        {
            // портретный вид
            camera.setDisplayOrientation(90);
            lp.height = previewSurfaceHeight;
            lp.width = (int) (previewSurfaceHeight / aspect);
        }
        else
        {
            // ландшафтный
            camera.setDisplayOrientation(0);
            lp.width = previewSurfaceWidth;
            lp.height = (int) (previewSurfaceWidth / aspect);
        }

        preview.setLayoutParams(lp);
        camera.startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onClick(View v){
        if (v == startCam)
            camera.takePicture(null, null, null, this);
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        new SavePicturesInBackground().execute();
        camera.startPreview();
    }


    class SavePicturesInBackground extends AsyncTask<byte[], String, String>{

        @Override
        protected String doInBackground(byte[]... params) {
            String pathToPhoto = null;
            try{
                pathToPhoto = new SimpleDateFormat("dd_MM_yyyy-HH_mm_ss").format(new Date());

                FileOutputStream os = new FileOutputStream(String.format("/storage/emulated/0/Pictures/%s.jpg", pathToPhoto));
                pathToPhoto = "/storage/emulated/0/Pictures/" + pathToPhoto + ".jpg";

                os.write(params[0]);
                os.close();
            }
            catch (Exception e) {
            }

            DB myDB = new DB(MyCamera.this, "photoDB.db", null, 1);
            SQLiteDatabase SDB = myDB.getReadableDatabase();

            if(SDB.isOpen() && pathToPhoto != null)
                //SDB.execSQL("INSERT INTO photoTable (photoColumn) VALUES ('" + pathToPhoto + "');");
                Toast.makeText(MyCamera.this, "БД открыта", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(MyCamera.this, "БД не открыта", Toast.LENGTH_SHORT).show();

            return(null);
        }
    }


    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {

    }
}
